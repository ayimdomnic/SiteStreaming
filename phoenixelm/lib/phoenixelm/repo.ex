defmodule Phoenixelm.Repo do
  use Ecto.Repo, otp_app: :phoenixelm
  use Scrivener, page_size: 9
end
