# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :phoenixelm,
  ecto_repos: [Phoenixelm.Repo]

# Configures the endpoint
config :phoenixelm, Phoenixelm.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "vVX/mTRg/mBEEyqtUxOufFpOJDD+02Z+mbvSiqxGdjyGw5lqwt1uGg4TZn1Tqsy6",
  render_errors: [view: Phoenixelm.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Phoenixelm.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
