defmodule Phoenixelm.ContactView do
    use Phoenixelm.Web, :view
    def render("index.json", %{page: page}), do: page
end