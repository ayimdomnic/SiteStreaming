defmodule Phoenixelm.ContactsController do
    use Phoenixelm.Web, :controller
    def index(conn, params) do
    page = Contact
      |> order_by(:first_name)
      |> Repo.paginate(params)

    render conn, page: page
  end

end